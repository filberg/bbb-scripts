

  To use the beaglebone as a WiFi DONGLE:

0) Connect the BBB through USB:
   Usb interface usb0 (on the bbb) has 192.168.7.2/24
   Usb interface usb1 (in the bbb) has 192.168.6.2/24

   On the client PC, interfaces are called somthing like this:

     ON THE PC       <=>     ON THE BBB
   enx4c3fd33ac5f6    =         usb0
   enx4c3fd33ac5f8    =         usb1


   On the PC, assign local IP 192.168.7.1/24 (if using usb0)
   or 192.168.6.1/24 (if using usb1):
   # ip addr add 192.168.7.1 dev enx4c3fd33ac5f6

   Add a static route to the BBB:
   # ip route add 192.168.7.2 dev enx4c3fd33ac5f6 scope link

   Connect to the BBB via ssh (default password = 'temppwd')
   $ ssh debian@192.168.7.2


1) ON THE BBB:

   Create a wifi connection:
   # wpa_passphrase <SSID> [Passphrase] > mynetwork.wpa

   Initialize wpa_supplicant:
   # wpa_supplicant -iwlan0 -B -c mynetwork.wpa

   Obtain IP address:
   # dhclient

   Enable ip forwarding:
   # echo 1 > /proc/sys/net/ipv4/ip_forward

   Enable DNAT (masquerading):
   # iptables --table nat -A POSTROUTING -o wlan0 -j MASQUERADE


2) ON THE PC:

   Set the BBB as default gateway:
   # ip route add default via 192.168.7.2

   Set up a DNS server
   (if using Replicant's hotspot, use the phone's IP as DNS server):
   # echo "nameserver 192.168.43.1" > /etc/resolv.conf

