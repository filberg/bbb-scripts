#!/bin/sh

usg="  USAGE: $0 < usb0 | usb1 >"

[ ! $# = 1 ] &&
    {
	echo "$usg"
	exit 255
    }

case "$1" in
    usb1)
	iface="enx4c3fd33ac5f8"
	addr_own="192.168.6.1"
	addr_beagle="192.168.6.2"
	lan_one="192.168.1.0/24"
	;;
    usb0)
	iface="enx4c3fd33ac5f6"
	addr_own="192.168.7.1"
	addr_beagle="192.168.7.2"
	lan_one="192.168.1.0/24"
	;;	
    *)
	echo "$usg"
	exit 255
	;;
esac

sudo ifconfig $iface up ||
    {
	echo "FAILED to bring up interface $iface"
	exit 1
    }

ip addr show dev $iface | grep -q $addr_own ||
    {
	sudo ip addr add $addr_own dev $iface ||
	    {
		echo "FAILED to add address $addr_own to $iface"
		exit 2
	    }
    }

sudo ip route add $addr_beagle dev $iface scope link

# sudo ip route del $lan_one

# sudo ip route add $lan_one via $addr_own dev $iface ||
#     {
# 	echo "FAILED to add route to $addr_beagle"
# 	exit 3
#     }

echo "Connected to beagleboard $addr_beagle!"
